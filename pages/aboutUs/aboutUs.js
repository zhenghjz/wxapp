// pages/aboutUs/aboutUs.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Animals:"",
    Plants:"",
    Genus:"",
    Species:"",
    branch:""
  },
  getAnimalsCount(res){
    this.setData({
      Animals:res.data.message
    })
  },
  getPlantsCount(res){
    this.setData({
      Plants:res.data.message
    })
  },
  getGenusCount(res){
    this.setData({
      Genus:res.data.message
    })
  },
  getSpeciesCount(res){
    this.setData({
      Species:res.data.message
    })
  },
  getBranchCount(res){
    this.setData({
      branch:res.data.message
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.$http.get(app.globalData.baseUrl + "directory/animals/animalsCount",this.getAnimalsCount)
    app.$http.get(app.globalData.baseUrl + "directory/plants/plantsCount",this.getPlantsCount)
    app.$http.get(app.globalData.baseUrl + "directory/genus/genusCount",this.getGenusCount)
    app.$http.get(app.globalData.baseUrl + "directory/species/genusCount",this.getSpeciesCount)
    app.$http.get(app.globalData.baseUrl + "directory/branch/branchCount",this.getBranchCount)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: '关于我们',
    })
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})