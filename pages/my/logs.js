var app = getApp()
Page({
  data: {
    date: '',
    show: false,
    hasUserInfo:false,
    userInfo:''
  },
  getUserProfile(e) {
    if(!wx.getStorageSync('userInfo')){
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        app.globalData.userInfo = this.data.userInfo
        wx.login({
          success (res) {
            if (res.code) {
              console.log(res.code)
              //发起网络请求
              wx.request({
                url:'http://47.118.66.86:8181/directory/user/userLogin',
                data:{
                  code: res.code
                },
                success: function (res){
                  console.log(res)
                  if(res.data.message.openid){
                  wx.setStorageSync('openId',res.data.message.openid);
                  wx.setStorageSync('session_key', res.data.message.session_key);
                  app.globalData.userInfo.openId = wx.getStorageSync('openId'),
                  app.register()
                  }
             },
             fail: function (res) {
              console.log(res)
         }
              })
            } else {
              console.log('登录失败！' + res.errMsg)
            }
          }
        })
      }
    })
  }else{
    console.log("用户已注册")
  }
  },
  onReady: function (){
    if(wx.getStorageSync('userInfo')){
      this.setData({
        userInfo:wx.getStorageSync('userInfo')
      })
    }
    console.log(wx.getStorageSync('userInfo'))
       },
  onDisplay() {
    this.setData({ show: true });
  },
  onClose() {
    this.setData({ show: false });
  },
  formatDate(date) {
    date = new Date(date);
    return `${date.getMonth() + 1}/${date.getDate()}`;
  },
  onConfirm(event) {
    this.setData({
      show: false,
      date: this.formatDate(event.detail),
    });
  },
  gomycom:function(){
    wx.navigateTo({
      url: '../myComment/myComment',
    })
  },
  goAboutUs:function(){
    wx.navigateTo({
      url: '../aboutUs/aboutUs',
    })
  }
});