// pages/home/home.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
   active:'home',
   temData:{
     silceData:"",
    cellBoxData:{
      "image":"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2484396802,3147108783&fm=26&gp=0.jpg",
      "name":"兔"
    },
    Carousel:"",
    isPlant: true
} ,
 },
 jumpTo(){
  wx.navigateTo({
    url: '/pages/inside/inside',
  })
},
jumpToinside(){
  wx.navigateTo({
    url: '/pages/inside/inside',
  })
},
jumpTodetail(){
  wx.navigateTo({
    url: '/pages/detailPagecopy/detailPage',
  })
},
  
  onChange(event){
    switch(event.detail.index){
      case 0 : app.$http.get(app.globalData.baseUrl + "directory/branch/searchPlantBranch",this.getData);
      app.globalData.isPlant = true
      this.setData({
        isPlant :  app.globalData.isPlant,
      })
      this.jumpTo = this.jumpToinside
      break;
      case 1 : app.$http.get(app.globalData.baseUrl + "directory/animals/searchAnimals",this.getData)
      app.globalData.isPlant = false;
      this.setData({
        isPlant : app.globalData.isPlant
      })
      this.jumpTo = this.jumpTodetail
      break;
    }
  },
  getData(res){
    if(res.data.status == 200){
      this.setData({
        'temData.silceData':res.data.message
      })
      // console.log(this.data.temData.silceData)
    }
  },
  getCarouselData(res){
    if(res.data.status == 200){
      this.setData({
        Carousel:res.data.message
      })
      // console.log(this.data.temData.silceData)
    }
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    app.$http.get(app.globalData.baseUrl + "directory/branch/searchPlantBranch",this.getData)
    app.$http.get(app.globalData.baseUrl + "directory/carousel/queryCarousel",this.getCarouselData)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setTabBarStyle({
      backgroundColor: '#ffffff',
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  methods:{
    jump(){
      wx.navigateTo({
        url: '/pages/detailPage/detailPage',
      })
    },
  }
})