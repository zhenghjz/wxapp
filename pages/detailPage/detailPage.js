// pages/detailPage/detailPage.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Fdata:"",
    plantName:""
  },
  getPlantData(res){
    console.log(res)
    if(res.data.message == null){
      app.$http.get(app.globalData.baseUrl + "directory/animals/queryAnimalById?id=" + app.globalData.animalId,this.getAnimalData)
    }else{
      this.setData({
        Fdata:res.data.message
      })
    }
    // app.globalData.plantId = ""
  },
  getAnimalData(res){
    // console.log(res)
      if(res.data.message == null){
        app.$http.get(app.globalData.baseUrl + "directory/plants/queryPlantById?id=" + app.globalData.plantId,this.getPlantData)
      }else{
        this.setData({
          Fdata:res.data.message
        })
      }
    // app.globalData.animalId = ""
  },
  ToSuggest(){
    wx.navigateTo({
      url: '/pages/message board/messageBoard',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.setNavigationBarTitle({
      title: '详情'
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if(app.globalData.plantId){
      app.$http.get(app.globalData.baseUrl + "directory/plants/queryPlantById?id=" + app.globalData.plantId,this.getPlantData)
    }else if(app.globalData.animalId || this.data.Fdata == null){
        app.$http.get(app.globalData.baseUrl + "directory/animals/queryAnimalById?id=" + app.globalData.animalId,this.getAnimalData)
    }
    },
    

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})