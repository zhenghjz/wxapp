// pages/inside/inside.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeKey: 0,
    deviceHight:"",
    Fdata:'',
    SpeciesData:'',
    GenusOne:''
  },
  getGenusData(res){
    console.log(res)
    if(res.data.status == 200){
      this.setData({
        Fdata:res.data.message,
        GenusOne:res.data.message[0].id
      })
      if(this.data.GenusOne){
        app.$http.get(app.globalData.baseUrl + "directory/species/searchSpeciesByGenusId?id=" + this.data.GenusOne,this.getSpeciesData)
      }
    }
  },
  getSpeciesData(res){
    if(res.data.status == 200){
      this.setData({
        SpeciesData:res.data.message,
        GenusOne:""
      })
    }
  },
  jump(){
    wx.navigateTo({
      url: '/pages/detailPage/detailPage',
    })
  },
  bindTap(e){
      app.globalData.genusId = e.target.dataset.id;
      if(app.globalData.genusId){
        app.$http.get(app.globalData.baseUrl + "directory/species/searchSpeciesByGenusId?id=" + app.globalData.genusId,this.getSpeciesData)
      }
  },
  onChange(index) { 
    console.log(index)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(wx.getSystemInfoSync().windowHeight){
      this.setData({
        deviceHight:wx.getSystemInfoSync().windowHeight
      })
    };
    if(app.globalData.sectionId){
      app.$http.get(app.globalData.baseUrl + "directory/genus/searchGenusByBranchId?id=" + app.globalData.sectionId,this.getGenusData)

    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: '属',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})