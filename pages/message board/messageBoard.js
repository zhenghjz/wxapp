// pages/message board/messageBoard.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message:"",
    fileList: [
     
      // Uploader 根据文件后缀来判断是否为图片文件
      // 如果图片 URL 中不包含类型信息，可以添加 isImage 标记来声明
    ],
    imgUrl:"",
    messageId:""
  },
  previewImage(){
    wx.previewImage({
      current: '',
      urls: [this.data.imgUrl],
    })
  },
  clearImg(){
    this.setData({
      imgUrl:""
    })
  },
  uploadImg(event){
    var that = this
    // console.log(event)
    // console.log(wx.getStorageSync('userInfo').openId)
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success (res) {
        // tempFilePath可以作为img标签的src属性显示图片
        console.log(res)
        that.setData({
          imgUrl:res.tempFilePaths[0]
        })
        wx.uploadFile({  
          url: 'http://47.118.66.86:8181/directory/user/userAddImages' , 
          filePath: res.tempFilePaths[0],
          name: 'file',  
          header: {  
          "Content-Type": "multipart/form-data"  
          },  
          formData:{ 
            openId:wx.getStorageSync('userInfo').openId, 
          },  
          success: function (res) {  
            let data = JSON.parse(res.data) 
            console.log(data)
            that.setData({
              messageId:data.message
            })
          }  
        }) 
      }
    }) 
  },
  outputMessage(){
    console.log(typeof(this.data.message))
    var that = this;
    wx.request({
      url: 'http://47.118.66.86:8181/directory/user/userAddMessage',
      method:'GET',
      data:{
        message:this.data.message,
        openId:wx.getStorageSync('userInfo').openId,
        messageId: this.data.messageId ? this.data.messageId : ""
      },
      success:function(res){
          if(res.data.message == "留言成功"){
              wx.showToast({
                title: '留言成功'
              })
              that.setData({
                message:""
              })
              wx.switchTab({
                url: '/pages/suggest/suggest',
              })
          }
      }
    })
  console.log(wx.getStorageSync('userInfo').openId)
  console.log(this.data.messageId)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '发布留言',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if(wx.getStorageSync('userInfo')){
      return
    }else{
      wx.showToast({
        title: '请先登录!',
        icon: 'error'
      })
      // wx.switchTab({
      //   url: '/pages/my/logs',
      // })
      setTimeout(function () { 
        wx.switchTab({
          url: '/pages/my/logs',
        })
     }, 1000) 

    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})