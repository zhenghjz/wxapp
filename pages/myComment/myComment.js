// pages/myComment/myComment.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    suggests:""
  },
  getSuggests(res){
    if(res.data.status == 200){
      this.setData({
        suggests:res.data.message
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '我的评论',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    app.$http.get(app.globalData.baseUrl + "directory/user/getUserMessageById?openId="+wx.getStorageSync('userInfo').openId,this.getSuggests)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})