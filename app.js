// app.js
App({
  onLaunch() {
    // 展示本地存储能力
    this.globalData.userInfo = wx.getStorageSync('userInfo')
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
      const that = this;
      // 获取系统信息
      const systemInfo = wx.getSystemInfoSync();
      // 胶囊按钮位置信息
      const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
      // 导航栏高度 = 状态栏到胶囊的间距（胶囊距上距离-状态栏高度） * 2 + 胶囊高度 + 状态栏高度
      that.globalData.navBarHeight = (menuButtonInfo.top - systemInfo.statusBarHeight) * 2 + menuButtonInfo.height + systemInfo.statusBarHeight;
      that.globalData.menuRight = systemInfo.screenWidth - menuButtonInfo.right;
      that.globalData.menuBotton = menuButtonInfo.top - systemInfo.statusBarHeight;
      that.globalData.menuHeight = menuButtonInfo.height;
      that.globalData.menuTop = menuButtonInfo.top
      that.globalData.meunWidth = menuButtonInfo.width
      this.$http.get(this.globalData.baseUrl + 'directory/user/getNewestTwentyBiological',this.getNewadd)
  },
  getRegister(res){
    console.log(res)
    wx.setStorageSync('userInfo',this.globalData.userInfo)
  },
  register(){
    this.$http.post("https://hutao.ink/a/directory/user/getUserInfo",{
      "openId": wx.getStorageSync('openId'),
      "nickName":this.globalData.userInfo.nickName,
      "imageUrl":this.globalData.userInfo.avatarUrl,
      "gender":this.globalData.userInfo.gender,
      "city":this.globalData.userInfo.city,
      "province":this.globalData.userInfo.province,
      "country":this.globalData.userInfo.country
    },this.getRegister)
  },
  globalData: {
    userInfo: null,
    hasLogin:false,
    baseUrl:"https://hutao.ink/a/",
    navBarHeight: 0, // 导航栏高度
    menuRight: 0, // 胶囊距右方间距（方保持左、右间距一致）
    menuBotton: 0, // 胶囊距底部间距（保持底部间距一致）
    menuHeight: 0, // 胶囊高度（自定义内容可与胶囊高度保证一致）
    meunwidth:0,
    menuTop:0,
    sectionId:'',
    genusId:'',
    speciesId:'',
    organismsId:'',
    animalId:'',
    plantId:'',
    newAdd:'',
    isPlant:""
  },
  getNewadd(res){
    let New = []
    for(let i = 0;i<=res.data.message.length-1;i++){
      New.push({title:res.data.message[i].plantName,id:res.data.message[i].id})
    }
    this.globalData.newAdd = New
  },
  $http:{
    get:function(url,callback) {
      wx.request({
        url:url,
        method:"get",
        success:function (res) {
          callback(res)
        },
        fail:function (res) {
          wx.showToast({
            title: "网络连接超时",
            icon: 'none',
            duration: 5000,
            })
        }
      })
    },
   post(url,data,callback) {
      wx.request({
        url: url,
        method:"POST",
        data:data,
        success:function (res) {
          callback(res)
        },
        fail:function (res) {
          wx.showToast({
            title: "网络连接超时",
            icon: 'none',
            duration: 5000,
            })
        }
      })
    }
  }
})
