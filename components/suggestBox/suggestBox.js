// components/suggestBox/suggestBox.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    Data:{
      type:Object,
      value:''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    heart:"like-o",
    Fdata:''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    loving(){
      this.setData({
        heart: this.data.heart == "like" ? "like-o" : "like"
      })
    }
  },
  observers: {
    'Data': function (newval) {
      this.setData({
        Fdata:newval
      })
    }
  },
})
