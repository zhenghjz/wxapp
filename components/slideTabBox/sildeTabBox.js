// component/slideTabBox/sildeTabBox.js
const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
    observers: {
      'Outdata': function(newval) {
          this.setData({
            Fdata:newval
          }) 
      }
  },
  properties: {
    Outdata:{
      type:Array,
      value:""
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    Fdata:[]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    bindTap(e){
        app.globalData.sectionId = e.currentTarget.dataset.id
        app.globalData.animalId = e.currentTarget.dataset.id
    }
  },

})
