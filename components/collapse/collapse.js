// components/collapse/collapse.js
const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    Outdata:{
      type:Array,
      value:""
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    activeName: '1',
    Fdata:'',
    Plants:''
  },
  observers: {
    'Outdata': function(newval) {
        this.setData({
          Fdata:newval
        }) 
    }
},

  /**
   * 组件的方法列表
   */
  methods: {
    onChange(event) {
      this.setData({
        activeName: event.detail,
      });
      if(app.globalData.speciesId != event.detail ){
        app.globalData.speciesId = event.detail;
        app.$http.get(app.globalData.baseUrl + "directory/plants/searchPlantsByGenusId?id=" +app.globalData.speciesId , (res)=> this.getPlantData(res))
      }
    },
    jump(e){
      wx.navigateTo({
        url: '/pages/detailPage/detailPage',
      })
      if(e.target.dataset.id){
        app.globalData.plantId = e.target.dataset.id
      }
  },
    getPlantData(res){
      this.setData({
        Plants:res.data.message
      })
    }
 }
})
