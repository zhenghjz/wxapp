// components/insideCell/insideCell.js
const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    Outdata:{
      type:Array,
      value:""
    }
  },
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  observers: {
    'Outdata': function(newval) {
        this.setData({
          Fdata:newval
        }) 
    }
},
  /**
   * 组件的初始数据
   */
  data: {
    Fdata:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    bindTap(e){
      app.globalData.speciesId = e.currentTarget.dataset.id
      console.log(app.globalData.speciesId)
    }
  }
})
