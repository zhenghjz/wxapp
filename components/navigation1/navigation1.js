// components/navigation1/navigation1.js
let app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    navBarHeight: app.globalData.navBarHeight,
    menuRight: app.globalData.menuRight,
    menuBotton: app.globalData.menuBotton,
    menuHeight: app.globalData.menuHeight,
    meunTop: app.globalData.menuTop
  },

  /**
   * 组件的方法列表
   */
  methods: {
    jump(){
      wx.navigateTo({
        url: '../../pages/searchPage/search',
      })
    }
  }
})
